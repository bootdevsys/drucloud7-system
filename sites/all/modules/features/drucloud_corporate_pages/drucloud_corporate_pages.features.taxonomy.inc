<?php
/**
 * @file
 * drucloud_corporate_pages.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drucloud_corporate_pages_taxonomy_default_vocabularies() {
  return array(
    'media' => array(
      'name' => 'Media',
      'machine_name' => 'media',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
